/**
 * Created by akshay on 28/5/16.
 */

'use strict';

var module=angular.module("dashboard",['ngRoute', 'textAngular', 'pageDirectives', 'ui.bootstrap']);

module.config(["$routeProvider",
    function($routeProvider){
        $routeProvider.
            when("/dashboard", {
                templateUrl:"dashboard.html"
            }).
            when("/contacts",{
                templateUrl:"contactsAdmin.html",
                controller:'ContactController'
            }).
            when("/about",{
                templateUrl:"aboutAdmin.html",
                controller:'AboutController'
            }).
            when("/customerContacts",{
                templateUrl:"partials/contactsGrid.html",
                controller:'CustomerContactsController'
            }).
            when("/customerContacts:contactsId",{
                templateUrl:"partials/contactsGrid.html"
            }).
            when("/reviews",{
                templateUrl:"partials/reviewsGrid.html",
                controller: 'ReviewsController'
            }).
            when("/album",{
                templateUrl:"partials/albumImages.html",
                controller: 'albumController'
            }).
            when("/scroller",{
                templateUrl:"partials/scrollerImages.html",
                controller: 'scrollerController'
            }).
            when("/background",{
                templateUrl:"partials/backgroundImage.html"
            }).
            otherwise({redirectTo:"/dashboard"})
    }]);
