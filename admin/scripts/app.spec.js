/**
 * Created by akshay on 31/5/16.
 */
'use strict'

describe("PhoneListController", function(){
   beforeEach(module("dashboard"));
    it("should create controller", inject(function($controller){
        var scope={};
        var ctrl= $controller("PhoneListController",{$scope:scope});
        expect(scope.phones.length).toBe(3);
    }));
});