/**
 * Created by akshay on 14/8/16.
 */
angular.module("dashboard")
    .controller("AboutController", ["$scope", "GetServices", function($scope, GetServices){
        var getUrl="php/get/getAbout.php";
        var postUrl="php/admin/post/PostAbout.php"
        $scope.aboutData={};
        var successCallBack=function(data){
            $scope.aboutData.about=data.data[0].about;
            $scope.aboutData.remarks=data.data[0].remarks;
        }
        var failureCallBack=function(data){
            alert("Sorry, Some error occured!!");
        }
        GetServices.getUrl(getUrl, "", successCallBack, failureCallBack);

        var postSuccessCallBack=function(data){

        }
        var postFailureCallBack=function(err){

        }

        $scope.postData=function(){
            GetServices.postUrl("php/admin/post/login/Login.php", /*[$scope.aboutData]*/[{username:"akshay", password:"akshay"}], successCallBack, failureCallBack);
        }
}]);