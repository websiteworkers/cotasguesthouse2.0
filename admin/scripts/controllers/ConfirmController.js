/**
 * Created by akshay on 2/9/16.
 */
angular.module("dashboard").
    controller("ConfirmController", ["$scope", "$uibModalInstance", "$rootScope", "review", "message", function($scope, $uibModalInstance, $rootScope, review, message){
        $scope.message=message;
        $scope.ok= function(){
            $rootScope.$emit("confirmDelete", {message: "confirmDelete", data: review});
            $uibModalInstance.close();
        }
        $scope.cancel= function(){
            $uibModalInstance.close();
        }
}]);