/**
 * Created by akshay on 15/8/16.
 */
angular.
    module("dashboard").
        controller("ContactController", ["GetServices", "$scope", "$http", function(GetServices, $scope, $http){
        var getUrl="php/get/getContact.php";
        var postUrl="php/admin/post/ContactPost.php";
        $scope.contactData={};
        var successCallBack= function(data){
            $scope.contactData.address=data.data[0].address;
            $scope.contactData.contact1=data.data[0].contact1;
            $scope.contactData.contact2=data.data[0].contact2;
            $scope.contactData.contact3=data.data[0].contact3;
            $scope.contactData.email=data.data[0].email;
            $scope.contactData.facebooklink=data.data[0].facebooklink;
            $scope.contactData.googlepluslink=data.data[0].googlepluslink;
            $scope.contactData.twtterlink=data.data[0].twtterlink;
        }
        var failureCallBack= function(err){

        }
        GetServices.getUrl(getUrl,null,successCallBack, failureCallBack);

        $scope.postContactData= function(){
            GetServices.postUrl(postUrl,[$scope.contactData], successCallBack, failureCallBack);
        }
    }]);