/**
 * Created by akshay on 16/8/16.
 */
angular.
    module("dashboard").
    controller("CustomerContactsController", ["GetServices", "$scope", "$http", "$rootScope", "$uibModal", function(GetServices, $scope, $http, $rootScope, $uibModal){
        var getUrl="php/admin/get/GetCustomerContacts.php";
        var postUrl="php/admin/post/MarkMessagesRead.php";
        var deleteMessages="php/admin/post/DeleteMessages.php";
        var successCallBack= function(data){
            $scope.customerContacts= data.data;
            $scope.customerContacts.forEach(function(rec, index){
               $scope.customerContacts[index].check=false;
            });
        }

        var failureCallBack= function(err){
            openModal(err, "noSelectionPrompt", "An error occured while doing this action");
        }

        GetServices.getUrl(getUrl, null, successCallBack, failureCallBack);

        $scope.$watch("selector", function(){
            if($scope.selector=="selectAll"){
                $scope.customerContacts.forEach(function(rec, index){
                    $scope.customerContacts[index].check=true;
                });
            } else {
                $scope.customerContacts.forEach(function(rec, index){
                    $scope.customerContacts[index].check=false;
                });
            }
        });

        $scope.markAsRead= function(){
            var checkSelect;
            $scope.customerContacts.forEach(function(rec, index){
                if($scope.customerContacts[index].check==true){
                    checkSelect=true;
                }
            });
            if(!checkSelect){
                openModal(null, "noSelectionPrompt");
            } else {
                GetServices.postUrl(postUrl, $scope.customerContacts, successCallBack, failureCallBack);
            }
        }

        $scope.deleteSelected=function(){
            //GetServices.postUrl(deleteMessages, $scope.customerContacts, successCallBack, failureCallBack);
            var checkSelect;
            $scope.customerContacts.forEach(function(rec, index){
                if($scope.customerContacts[index].check==true){
                    checkSelect=true;
                }
            });
            if(checkSelect){
                openModal($scope.customerContacts, "confirmDialogue");
            } else {
                openModal($scope.customerContacts, "noSelectionPrompt");
            }

        }
        $rootScope.$on('confirmDelete', function (event, args) {
            if(args.message=='confirmDelete' && args.data){
                GetServices.postUrl(deleteMessages, $scope.customerContacts, successCallBack, failureCallBack);
            }
        });

        var openModal=function(data, template, message){
            $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: template,
                controller: "ConfirmController",
                size: 'md',
                windowClass : "modalConfirm",
                resolve: {
                    review: function(){
                        return data;
                    },
                    message: function(){
                        return message;
                    }

                }
            });
        }
}]);