/**
 * Created by akshay on 15/8/16.
 */
angular.
    module("dashboard").
    controller("ReviewsController", ["GetServices", "$scope", "$http", "$uibModal", "$rootScope", function(GetServices, $scope, $http, $uibModal, $rootScope){
        var getUrl="php/get/getReviews.php";
        var postUrl="php/admin/post/MarkReviewsRead.php";
        var deleteReviews="php/admin/post/DeleteReview.php";
        var successCallback= function(data){
            $scope.reviewsData= data.data;
            $scope.reviewsData.forEach(function(rec, index){
                $scope.reviewsData[index].check=false;
            });
        };
        var failureCallBack= function(err){

        };
        GetServices.getUrl(getUrl, null, successCallback, failureCallBack);

        $scope.toggleSelect=function(){

        }

        $scope.$watch("selector", function(){
            if($scope.selector=="selectAll"){
                $scope.reviewsData.forEach(function(rec, index){
                    $scope.reviewsData[index].check=true;
                });
            } else if($scope.selector=="unSelectAll") {
                $scope.reviewsData.forEach(function(rec, index){
                    $scope.reviewsData[index].check=false;
                });
            }
        });

        $scope.markAsRead= function(){
            var checkSelect;
            $scope.reviewsData.forEach(function(rec, index){
                if($scope.reviewsData[index].check==true){
                    checkSelect=true;
                }
            });
            if(!checkSelect){
                openModal($scope.reviewsData, "noSelectionPrompt");
            } else {
                GetServices.postUrl(postUrl, $scope.reviewsData, successCallback, failureCallBack);
            }
        }

        $scope.deleteSelected=function(){
            //GetServices.postUrl(deleteReviews, $scope.reviewsData, successCallback, failureCallBack);
            var checkSelect;
            $scope.reviewsData.forEach(function(rec, index){
                if($scope.reviewsData[index].check==true){
                    checkSelect=true;
                }
            });
            if(checkSelect){
                openModal($scope.reviewsData, "confirmDialogue");
            } else {
                openModal($scope.reviewsData, "noSelectionPrompt");
            }

        }

        $rootScope.$on('confirmDelete', function (event, args) {
            if(args.message=='confirmDelete' && args.data){
                GetServices.postUrl(deleteReviews, args.data, successCallback, failureCallBack);
            }
        });

        $scope.delete=function(review){
            review.check=true;
            openModal([review], "confirmDialogue");
            //GetServices.postUrl(deleteReviews, [review], successCallback, failureCallBack);
        }
        var openModal=function(data, template, message){
            $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: template,
                controller: "ConfirmController",
                size: 'md',
                windowClass : "modalConfirm",
                resolve: {
                    review: function(){
                        return data
                    },
                    message: function(){
                        return message;
                    }
                }
            });
        }

}]);