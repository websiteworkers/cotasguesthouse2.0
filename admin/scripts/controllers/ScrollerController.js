/**
 * Created by akshay on 30/7/16.
 */
angular.module("dashboard").controller(
    "scrollerController", function($scope, $location, $http){
        var absurl=$location.$$absUrl.slice(0,$location.$$absUrl.indexOf('admin'));
        $http({
            method:'get',
            url: absurl+'php/get/getCarouselImages.php'
        }).then(function success(response){
            var rawData=response.data;
            $scope.images=[];
            rawData.data.forEach(function(img){
                if(img.substr(img.lastIndexOf(".")+1).length>2){
                    $scope.images.push({imgSrc:img.replace("../../","../"), imageSelected: false, imageDeleteFlag: false});
                }
            });
            console.log($scope.images);
        }, function failure(){
            $scope.errorMessage="Failed To retrieve data, Sorry for the inconvenience";
        });

        $scope.selectImage= function(imageOb){
            if(imageOb.imageSelected){
                imageOb.imageSelected=false;
            } else {
                imageOb.imageSelected=true;
            }
            $scope.$apply();
        }
    }
)