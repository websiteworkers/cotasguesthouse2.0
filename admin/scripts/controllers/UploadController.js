/**
 * Created by akshay on 5/9/16.
 */
angular.module("dashboard").
    controller("uploadController",["$scope","items", "$document", "$uibModalInstance", "$http", "$location", function($scope,items, $document, $uibModalInstance, $http, $location) {
        $scope.images=items;
        $scope.image_source=[];
        if(items.files.length>0){
            for(var i=0; i<items.files.length; i++){
                var reader=new FileReader();
                reader.onload = function(event) {
                    $scope.image_source.push(event.target.result);
                    $scope.$apply();
                }
                reader.readAsDataURL(items.files[i]);
            }
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        var absurl=$location.$$absUrl.slice(0,$location.$$absUrl.indexOf('admin'));
        $scope.post = function(){
            if(items.files.length>0) {
                for (var i = 0; i < items.files.length; i++) {
                    var data= new FormData();
                    data.append('file', items.files[i]);
                    $http({
                        method:'post',
                        url: absurl+'php/admin/post/images/postAlbum.php',
                        data: data,
                        headers: {'Content-Type': undefined},
                        progressall: function (e, data) {
                            status.progress = parseInt(data.loaded / data.total * 100, 10);

                        }
                    }).then(function success(response){
                        var rawData=response.data;
                        console.log(rawData);
                    }, function failure(){
                        $scope.errorMessage="Failed To retrieve data, Sorry for the inconvenience";
                    }, function(evt){
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                    });
                }
            }
        }



}]);