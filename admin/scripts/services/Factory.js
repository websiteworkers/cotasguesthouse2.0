/**
 * Created by akshay on 14/8/16.
 */
angular.
    module('dashboard').
    factory("GetServices",function($location, $http){
        var absurl=$location.$$absUrl.slice(0,$location.$$absUrl.indexOf('admin'));
        return {
            getUrl : function(url, data, success, failure){
                $http({
                    url:absurl+url,
                    method: "GET",
                    data: data
                }).then(success, failure);

            },
            postUrl : function(url, data, success, failure){
                $http({
                    url:absurl+url,
                    method: "POST",
                    data: data
                }).then(success, failure);
            }
        }});
