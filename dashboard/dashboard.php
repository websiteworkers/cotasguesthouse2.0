<?php
session_start();
if(!isset($_SESSION['username']))
{
	header("location:login.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        	<title>Admin Dashboard</title>
       
        <link rel="stylesheet" type="text/css" href="dashboardstyle.css"> 
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
         </script>
       
        <script>

         // Wait until the DOM has loaded before querying the document
         $(document).ready(function(){

            //Active tab selection after page load       
            $('#tabs').each(function(){
           
            // For each set of tabs, we want to keep track of
            // which tab is active and it's associated content
            var $active, $content, $links = $(this).find('a');
           
            // If the location.hash matches one of the links, use that as the active tab.
            // If no match is found, use the first link as the initial active tab.
            $active = $($links.filter('[href="'+location.hash+'"]')[0]
                      || $links[0]);
            
            $active.parent().addClass('active');
           
            $content = $($active.attr('href'));
            $content.show();
        });
       
       
         $("#tabs li").click(function() {
                
             // First remove class "active" from currently active tab
             $("#tabs li").removeClass('active');
        
             // Now add class "active" to the selected/clicked tab
             $(this).addClass("active");
                
             // Hide all tab content
             $(".tab_content").hide();

             // Here we get the href value of the selected tab
             var selected_tab = $(this).find("a").attr("href");      

             var starting = selected_tab.indexOf("#");
             var sub = selected_tab.substring(starting);
               
             //write active tab into cookie
                
             //$(sub).show();
                 $(sub).fadeIn();
             // At the end, we add return false so that the click on the
            // link is not executed
             return false;
          });
        });

       </script>

		<script>
			function DeletePhoto(path) 
			{
				var r = confirm("Are yoy sure you want to delete the photo? " + path);
				if (r == true) 
				{
					getRequest(
					      'deletephoto.php?q=' + path, // URL for the PHP file
					       drawOutput,  // handle successful request
					       drawError    // handle error
					  );
				} 
				else 
				{

				}		
				//alert ("JS method called");
			}
			
			// handles drawing an error message
			function drawError () 
			{
				alert('Bummer: there was an error!');
			}
			
			// handles the response, adds the html
			function drawOutput(responseText) 
			{
				alert(responseText);
				location.reload();
			}
			
			function getRequest(url, success, error) 
			{
			    var req = false;
			    try
			    {
			        // most browsers
			        req = new XMLHttpRequest();
			    } 
			    catch (e)
			    {
			        // IE
			        try
			        {
			            req = new ActiveXObject("Msxml2.XMLHTTP");
			        } 
			        catch (e) 
			        {
			            // try an older version
			            try
			            {
			                req = new ActiveXObject("Microsoft.XMLHTTP");
			            } 
			            catch (e)
			            {
			                return false;
			            }
			        }
			    }
			    if (!req) return false;
			    if (typeof success != 'function') success = function () {};
			    if (typeof error!= 'function') error = function () {};
			    req.onreadystatechange = function()
			    {
			        if(req .readyState == 4)
			        {
			            return req.status === 200 ? 
			                success(req.responseText) : error(req.status)
			            ;
			        }
			    }
			    req.open("GET", url, true);
			    req.send(null);
			    return req;
			}
			
			function tabGalleryLoad()
			{
				alert ("Gallery loaded");
			}
			
		</script>


    </head>
    <body id="page-wrap">
    	
    	
    	<span>
    			<h3  >Admin's Dashboard</h3>
    			<a float="left" class="logoutLblPos" href="logout.php">Logout Admin</a>
    		
    	</span>
    		
        <ul id='tabs' class="tabss">
        	<li><a href='#VisitorList'>Visitor List</a></li>
        	<li><a href='#tab0'>Scroller Photos</a></li>
            <li><a href='#tab1'>Gallery Photos</a></li>
            <li><a href='#tab2'>Contact Information</a></li>
            <li><a href='#tab3'>About Us Information</a></li>
            <!--<li><a href='#tab4'>Tariff Information</a></li>
            <li><a href='#tab5'>Stay Information</a></li> -->
            <li><a href='#AccSetting'>Account Setting</a></li>
        </ul>
        
        <div id='VisitorList' class="tab_content" >
        	<h3>Visitors who contacted you recently (Recent 50)</h3>
        	<?php
        		//fetch the top visitors
        		include 'fetchvisitorlist.php';
        	?>
        	
        </div>	
       
        <div id='tab0' class="tab_content" >
        	<h3>Upload New photo</h3>
            <form action="uploadscrollerimgtoserver.php" method="post"	enctype="multipart/form-data">
				<label for="file">Filename:</label>
				<input type="file" name="file" id="file">
				<input type="submit" name="submit" value="Upload">
			</form>
		
			</br>	
		
			<h3>Photos currently in the scroller</h3>
			
				<?php
	            	include 'viewexistingscrollerphotos.php';
	            ?>
            
        </div>
        
        <div id='tab1' class="tab_content">
        	
	            <h3>Upload New photo</h3>
	            <form action="uploadimgtoserver.php" method="post"	enctype="multipart/form-data">
					<label for="file">Filename:</label>
					<input type="file" name="file" id="file">
					<input type="submit" name="submit" value="Upload">
				</form>
			
			</br>	
			
				<h3>Photos currently in the gallery</h3>
				
				<span>
					
					<?php
		            	include 'viewexistingphotos.php';
		            ?>
            	</span>
        </div>
       
        <div id='tab2' class="tab_content">
            <h3>Update contact information</h3>
            
           	<?php
           		include 'fetchexistingcontactinformation.php';
           		$breaks = array("<br />","<br>","<br/>","<br />","&lt;br /&gt;","&lt;br/&gt;","&lt;br&gt;");
		    ?> 
    		
           <form  action="updateaccomodationdata.php" method="post">
		      	
		      	<table style="width:100%">
		      	<col width="80">
 				<col width="180">
		      	<tr>
		      	  <td><label align="left" for="idAccomName">Name</label></td>
		          <td><input align="right" value=" <?php echo $accName ;?> " name="accomname" type="text" id="idAccomName" placeholder="Enter Accomodation Name"></td>
		        </tr>
		        <tr>
		          <td><label align="left" for="idAddress">Address</label></td>
		          <td>
		          
		          <textarea  class="form-control" name="address" id="idAddress" rows=10 cols=50 placeholder="Enter Address"><?php echo  str_ireplace($breaks, "\n", $address );?></textarea>
		          </td>
		        </tr>
		        <tr>
		        <td><label align="left" for="idEmail">email</label></td>
		          <td><input value="<?php echo  $email ;?>" name="email" type="text"  id="idEmail" placeholder="eMail"></td>
		        </tr>
		        </tr>
		        <td><label align="left" for="idContact1">Contact No. 1</label></td>
		          <td><input value="<?php echo  $contact1 ;?>" name="contact1" type="text" height='50'  id="idContact1" placeholder="Contact No. 1"></td>
		        </tr>
		        <tr>
		        <td><label align="left" for="idContact2">Contact No. 2</label></td>
		          <td><input value="<?php echo  $contact2 ;?>" name="contact2" type="text" height='50'  id="idContact2" placeholder="Contact No. 2"></td>
		        </tr>
		        <tr>
		        <td><label align="left" for="idContact3">Contact No. 3</label></td>
		          <td><input value="<?php echo  $contact3 ;?>" name="contact3" type="text" height='50'  id="idContact3" placeholder="Contact No. 3"></td>
		        </tr>
		        
		        <tr>
		        <td><label align="left" for="idContact4">Contact No. 4</label></td>
		          <td><input value="<?php echo  $contact4 ;?>" name="contact4" type="text" height='50'  id="idContact4" placeholder="Contact No. 4"></td>
		        </tr>
		        <tr>
		        <td><label align="left" for="idContact5">Contact No. 5</label></td>
		          <td><input value="<?php echo  $contact5 ;?>" name="contact5" type="text" height='50'  id="idContact5" placeholder="Contact No. 5"></td>
		        </tr>
		        </table>
		        <button type="submit" >Update</button>
		        
		      </form>
           
           
        </div>
        
        <div id='tab3' class="tab_content">
        	
    	<?php
       	
      	include 'fetchexistingaboutusinfo.php';
		
       	$breaks = array("<br />","<br>","<br/>","<br />","&lt;br /&gt;","&lt;br/&gt;","&lt;br&gt;");
	    ?> 
	       <form  action="updateaboutusdata.php" method="post">
	      	
	      
	      	<table style="width:100%">
	      	
	      	<tr>
	      	  <td><label for="idAbout">About</label></td>
	          <td>
	          	
	          	<!-- <input value=" <?php echo $about ;?>"  name="about" type="textarea" id="idAbout" placeholder="Enter About "> -->
	          	
	          	<textarea  class="form-control" name="about" id="idAbout" rows=10 cols=80 placeholder="Enter About"><?php echo  str_ireplace($breaks, "\n", $about) ;?></textarea>
	          	
	          </td>
	        </tr>
	        
	        <tr>
	      	  <td><label for="idAddress">Description</label></td>
	          <td>
	         <!-- <input value="<?php echo $remarks ;?>" rows="10"  name="remarks" type="textarea"  id="idRemarks" placeholder="Enter Description"></td> -->
		        <textarea  class="form-control" name="remarks" id="idRemarks" rows=10 cols=80 placeholder="Enter Description"><?php	echo  str_ireplace($breaks, "\n",  $remarks) ;?></textarea>
		        	
		        </td>
	        </tr>
	        
	        </table>
	      	
	        <button type="submit" >Update</button>
	      
	      </form>
        </div>
        
        
          <div id='tab4' class="tab_content">
          	
          	<?php
	         echo "<form action=\"updatetariffdata.php\" method=\"post\">
	         
					<table style=\"width:100%\">
					
						<tr>
				      	  <td><label>Category</label></td>
							<td>
							<select name = \"categoty\">
				         	  <option value=\"Normal\">Normal</option>
							  <option value=\"Semi Deluxe\">Semi Deluxe</option>
							  <option value=\"Deluxe\">Deluxe</option>
							  <option value=\"Ultra Deluxe\">Ultra Deluxe</option>
							</select>
							</td>
						</tr>
				      	<tr>
				      	  <td><label>Price for single</label></td>
				          <td><input name=\"PriceForSingle\" type=\"text\" id=\"idForSingle\" placeholder=\"Enter price for single \"></td>
				        </tr>
						<tr>
				      	  <td><label >Price for double</label></td>
				          <td><input name=\"PriceForDouble\" type=\"text\" id=\"idForDouble\" placeholder=\"Enter price for double \"></td>
				        </tr>
				        <tr>
				      	  <td><label >Description</label></td>
				          <td><input name=\"Description\" type=\"text\" id=\"idDescription\" placeholder=\"Enter description \"></td>
				        </tr>
				        <tr>
				      	  <td><label >More details</label></td>
				          <td><input name=\"MoreDetails\" type=\"text\" id=\"idMoreDetails\" placeholder=\"Enter more details \"></td>
				        </tr>
				        </table>
					<button type=\"submit\" >Update</button>
	         	</form>"
         	?> 
         	
         </div>	
         <div id='tab5' class="tab_content">
          	
	            <h3>Create New Facility</h3>
	            <form action="createNewFacilityData.php" method="post"
					enctype="multipart/form-data">
					<label >Facility name:</label>
					<input type="text" name="facilityName" id="idFacilityName">
					<input type="submit" name="submit" value="Create">
				</form>
			
			</br>	
			
				<h3>Update current Facilities</h3>
				
				<?php
				
	            include 'updateExistingFacilities.php';
	            
	            ?>
            
         </div>
         
          <div id='AccSetting' class="tab_content">
          	
	            <h3>Change Admin Password</h3>
	            <form action="resetPassword.php" method="post"	enctype="multipart/form-data">
					<table>
					<tr>
				        <td><label >Old Password:</label></td>
						<td><input type="password" name="oldPassword" id="idoldPassword"></td>
					</tr>
				    <tr>
						<td><label >Enter New Password:</label></td>
						<td><input type="password" name="newPasswordOne" id="idnewPasswordOne"></td>
					</tr>
				    <tr>
						<td><label >ReEnter New Password:</label></td>
						<td><input type="password" name="newPasswordTwo" id="idnewPasswordTwo"></td>
					</tr>
				       
					<tr>
						<td><label >You will be logged out! continue save? </label></td>
						<td><input type="submit" name="submit" value="Yes, Change my Password"></td>
					</tr>
					</table>
				</form>
				
				<h3>Change Admin Contact email</h3>
				<?php
				
				$emailExisting = "No email stored";
				include '../phpscripts/oc.php';
				//session_start();
				$userNAme =  $_SESSION["username"];
				$selectsql = "SELECT email FROM tbl_Users where userName = '$userNAme' AND userTypeId=0";

				if (!$result=mysqli_query($con,$selectsql)) 
				{
				  	die('Error: ' . mysqli_error($con));
				}
				else 
				{
					while($row=mysqli_fetch_row($result))	
					{
						$emailExisting = $row[0];
					}
				}
				include '../phpscripts/clsc.php';
				?>
	            <form action="updateAdminContact.php" method="post" enctype="multipart/form-data">
					<table>
					
				    <tr>
						<td><label >Enter email:</label></td>
						<td><input type="text" value="<?php echo $emailExisting ;?>" name="contactEmail" id="idcontactEmail"></td>
						<td><input type="submit" name="submit" value="Update"></td>
					</tr>
				</table>    
				
				</form>
			
         </div>
    </body>
</html>
