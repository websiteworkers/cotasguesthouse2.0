
<?php 
	ob_start();
	//ini_set('display_errors', 1); 
	//error_reporting(E_ALL); 

	session_start();
	if(session_status() == PHP_SESSION_ACTIVE)
	{
		if(isset($_SESSION['username']))
  			unset($_SESSION['username']);
		
		session_destroy();
	}
	include '../phpscripts/clsc.php';
	header("location:login.php");	
	ob_end_flush();	
?>
