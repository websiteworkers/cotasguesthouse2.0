<?php

ini_set('display_errors', 1); 
error_reporting(E_ALL); 

include '../phpscripts/oc.php';

//$about = mysqli_real_escape_string($con, $_POST['about']);


$selectsql = "select count(*) from tbl_Facility";

if (!$result=mysqli_query($con,$selectsql)) 
{
  	die('Error: ' . mysqli_error($con));
}
else 
{
	while($row=mysqli_fetch_row($result))	
	{
		if($row[0]!="0")
		{
			$selectStay = "select * from tbl_Facility";
			if (!$resultStay=mysqli_query($con,$selectStay)) 
			{
			  	die('Error: ' . mysqli_error($con));
			}
			else 
			{
				$buildFacilityOptions = "";
				while($rowOption=mysqli_fetch_assoc($resultStay))	
				{
					$buildFacilityOptions  .= "<option value=\"$rowOption[facility]\">$rowOption[facility]</option>";	  
				}
				$buildFacilitySelect = "<select name = \"facility\">" . $buildFacilityOptions . "</select>";
			 
			 	$dir = "../photos/";
	
				$imgnamearr = scandir($dir);
				$buildImageOptions1  = "<option value=\"\"></option>"; 
				$buildImageOptions2  = "<option value=\"\"></option>"; 
				$buildImageOptions3  = "<option value=\"\"></option>"; 
				$buildImageOptions4  = "<option value=\"\"></option>"; 
				foreach ($imgnamearr as &$value) 
				{
					$buildImageOptions1  .= "<option value=\"$value\">$value</option>"; 
					$buildImageOptions2  .= "<option value=\"$value\">$value</option>"; 
					$buildImageOptions3  .= "<option value=\"$value\">$value</option>"; 
					$buildImageOptions4  .= "<option value=\"$value\">$value</option>"; 
				}
				
				$buildImageSelect1 = "<select name = \"image1\">" . $buildImageOptions1 . "</select>";
				$buildImageSelect2 = "<select name = \"image2\">" . $buildImageOptions2 . "</select>";
				$buildImageSelect3 = "<select name = \"image3\">" . $buildImageOptions3 . "</select>";
				$buildImageSelect4 = "<select name = \"image4\">" . $buildImageOptions4 . "</select>";
				
				echo "<form  action=\"updateExistingFacilitiesData.php\" method=\"post\">";
				
				echo "<table class=\"table table-striped\">";
				echo "<tr>
		      	  		<td><label>Facility type </label></td>
		          		<td>$buildFacilitySelect</td>
		        	  </tr>";
			    echo "<tr>
		      	  		<td><label>Image 1 </label></td>
		          		<td>$buildImageSelect1</td>
		        	  </tr>"; 
			    echo "<tr>
		      	  		<td><label>Image 2 </label></td>
		          		<td>$buildImageSelect2</td>
		        	  </tr>"; 
				echo "<tr>
		      	  		<td><label>Image 3 </label></td>
		          		<td>$buildImageSelect3</td>
		        	  </tr>"; 
				echo "<tr>
		      	  		<td><label>Image 4 </label></td>
		          		<td>$buildImageSelect4</td>
		        	  </tr>";   
				echo "<tr>
		      	  		<td><label>Description </label></td>
		          		<td><input name=\"description\" type=\"text\" id=\"idDescription\" placeholder=\"Enter Description\"></td>
		        	  </tr>"; 
			    echo "</table>";
				
				echo "<button type=\"submit\" >Update</button>";
		        
		      	echo "</form>";     
			}
		}
		else
	    {
			echo "No Facilities currently created";
		}
	}
}

//close the connection
include '../phpscripts/clsc.php';

?>