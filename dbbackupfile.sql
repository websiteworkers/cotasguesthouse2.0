-- MySQL dump 10.13  Distrib 5.5.52, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cota
-- ------------------------------------------------------
-- Server version	5.5.52-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_AboutUs`
--

DROP TABLE IF EXISTS `tbl_AboutUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_AboutUs` (
  `id` int(11) NOT NULL DEFAULT '0',
  `about` varchar(1000) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_AboutUs`
--

LOCK TABLES `tbl_AboutUs` WRITE;
/*!40000 ALTER TABLE `tbl_AboutUs` DISABLE KEYS */;
INSERT INTO `tbl_AboutUs` VALUES (0,'<p>Cota Residency offers the best in affordable lodging. Just a short walk from the golden sands of Arossim beach. Enjoy the convenience of our location next to the Park Hyatt Goa Resort and Spa. We offer luxury accommodations at affordable prices!<br/><br/>Visit us on  :Face book- Cota Residency<br/>Review us on : Trip advisor- Cota Residency-Utorda<br/></p>','<p>Cota Residency includes beautifully appointed and well furnished Air conditioned suites. All suites offer the amenities you would expect, including a kitchenette, with a large fridge, microwave, electric kettle, dishes, flat screen TVs with cable package and independent balconies.<br/><br/>A warm and friendly family awaits you at the Cota Residency!<br/>12km from Dabolim Airport (GOI)<br/>10 minute walk from the beach<br/><b>Walking distance to local churches</b><br/>Walking distance to local /starred restaurants</p>');
/*!40000 ALTER TABLE `tbl_AboutUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Accommodation`
--

DROP TABLE IF EXISTS `tbl_Accommodation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Accommodation` (
  `id` int(11) NOT NULL DEFAULT '0',
  `accname` varchar(20) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `contact1` varchar(20) DEFAULT NULL,
  `contact2` varchar(20) DEFAULT NULL,
  `contact3` varchar(20) DEFAULT NULL,
  `contact4` varchar(20) DEFAULT NULL,
  `contact5` varchar(20) DEFAULT NULL,
  `facebooklink` varchar(1000) DEFAULT NULL,
  `twtterlink` varchar(1000) DEFAULT NULL,
  `googlepluslink` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Accommodation`
--

LOCK TABLES `tbl_Accommodation` WRITE;
/*!40000 ALTER TABLE `tbl_Accommodation` DISABLE KEYS */;
INSERT INTO `tbl_Accommodation` VALUES (0,'            ','Francis Costa Ward\n-Utorda, \nGoa, India','renoircota@gmail.com','0091-9822','123455656','','','','https://www.facebook.com/www.cotasguesthouse.in','https://twitter.com/CotaResidency','https://plus.google.com/103564331097818715048');
/*!40000 ALTER TABLE `tbl_Accommodation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_AdminInformation`
--

DROP TABLE IF EXISTS `tbl_AdminInformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_AdminInformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(20) DEFAULT NULL,
  `LastName` varchar(20) DEFAULT NULL,
  `Contact` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Address` varchar(200) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_AdminInformation`
--

LOCK TABLES `tbl_AdminInformation` WRITE;
/*!40000 ALTER TABLE `tbl_AdminInformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_AdminInformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ContactUs`
--

DROP TABLE IF EXISTS `tbl_ContactUs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ContactUs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custName` varchar(30) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(5000) DEFAULT NULL,
  `visitTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `readFlag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ContactUs`
--

LOCK TABLES `tbl_ContactUs` WRITE;
/*!40000 ALTER TABLE `tbl_ContactUs` DISABLE KEYS */;
INSERT INTO `tbl_ContactUs` VALUES (4,'Bruno Brugger','','brunobrugger@hotmail.com','We are coming to Majorda next week and would like to come and look at your property with view of booking for 2016.\r\nCould you please let me have your exact address in Utorda and/or  description how to find you and/or map...\r\nMany thanks\r\nBruno Brugger','2015-01-10 00:29:45',1),(5,'malcolm walker','01217056169','bassett11148&tiscali.co.uk','my partner and I have just returned from grand cayman were we met Renoir who gave us your details. I would like to know the cost of your accommodation for two weeks in November this year. We went to Baga Beach three years ago, I would like to know how far that is away. Also how far are the local resturants. ','2015-02-17 02:50:52',1),(6,'Vardaan Josan','9823400022','vjosan@hotmail.com','Dear All,\r\n\r\nRead the excellent comments about your property and you on Trip Advisor. My heartiest congratulations on the same. \r\n\r\nI am unable to get thru to your cell no - 98221 62106.\r\n\r\nI am looking to book a room in vicinity of Utorda for 19th April\' 15 night, check out on 20th morning, post breakfast.\r\n\r\nCould you kindly let me know the room availability, room expense and also the cost of airport pick-up on 19th April afternoon.\r\n\r\nI am travelling with my wife and a 3 year old daughter.\r\n\r\n\r\nRegards\r\n\r\nVardaan ','2015-02-18 19:38:47',1),(7,'daksh seth','9971338337','dakshseth17@gmail.com','hi we are a group of seven coming to goa on 22 april till 26 \r\ncan you let me know of some grat deals which you are having','2015-03-31 19:37:40',1),(8,'Victoire','Victoire','njkprevwrn@ymail.com','Hi, my name is Victoire and I am the marketing manager at SwingSEO Solutions. I was just looking at your Cota\'s Guest House site and see that your site has the potential to get a lot of visitors. I just want to tell you, In case you don\'t already know... There is a website network which already has more than 16 million users, and most of the users are looking for topics like yours. By getting your site on this network you have a chance to get your site more popular than you can imagine. It is free to sign up and you can find out more about it here: http://anders.ga/6uk1z - Now, let me ask you... Do you need your website to be successful to maintain your way of life? Do you need targeted visitors who are interested in the services and products you offer? Are looking for exposure, to increase sales, and to quickly develop awareness for your website? If your answer is YES, you can achieve these things only if you get your website on the service I am describing. This traffic network advertises you to thousands, while also giving you a chance to test the service before paying anything at all. All the popular blogs are using this network to boost their readership and ad revenue! Why arenâ€™t you? And what is better than traffic? Itâ€™s recurring traffic! That\'s how running a successful website works... Here\'s to your success! Find out more here: http://todochiapas.mx/C/369  - or to unsubscribe please go here: http://bbqr.me/n1','2015-04-24 08:43:20',1),(9,'Brigitte','Brigitte','uwntuum@ymail.com','Hi, my name is Brigitte and I am the sales manager at SwingSEO Solutions. I was just looking at your Cota\'s Guest House site and see that your website has the potential to become very popular. I just want to tell you, In case you didn\'t already know... There is a website service which already has more than 16 million users, and the majority of the users are looking for niches like yours. By getting your site on this service you have a chance to get your site more visitors than you can imagine. It is free to sign up and you can read more about it here: http://anders.ga/w-6x2 - Now, let me ask you... Do you need your website to be successful to maintain your way of life? Do you need targeted visitors who are interested in the services and products you offer? Are looking for exposure, to increase sales, and to quickly develop awareness for your website? If your answer is YES, you can achieve these things only if you get your site on the service I am describing. This traffic service advertises you to thousands, while also giving you a chance to test the service before paying anything at all. All the popular sites are using this service to boost their traffic and ad revenue! Why arenâ€™t you? And what is better than traffic? Itâ€™s recurring traffic! That\'s how running a successful website works... Here\'s to your success! Find out more here: http://jawaidali.com/redirectURL/3w  - or to unsubscribe please go here: http://todochiapas.mx/C/36p','2015-05-07 15:52:24',1),(10,'Naveen Serrao','7204429022','naveen.stephen@gmail.com','Hello,\r\nWe are a couple & will be arriving at 7:35 AM by flight on 2nd August 2015 & will be checking out at 6:30 AM on 6th August 2015. Request a quote for the same. Regards Naveen Serrao','2015-05-12 21:32:50',1),(11,'Delphine','Delphine','awywqso@yahoo.co.uk','Hi, my name is Delphine and I am the sales manager at SwingSEO Solutions. I was just looking at your Cota\'s Guest House website and see that your website has the potential to get a lot of visitors. I just want to tell you, In case you don\'t already know... There is a website service which already has more than 16 million users, and the majority of the users are interested in topics like yours. By getting your website on this network you have a chance to get your site more visitors than you can imagine. It is free to sign up and you can find out more about it here: http://t8k.me/wk - Now, let me ask you... Do you need your website to be successful to maintain your way of life? Do you need targeted traffic who are interested in the services and products you offer? Are looking for exposure, to increase sales, and to quickly develop awareness for your site? If your answer is YES, you can achieve these things only if you get your website on the network I am describing. This traffic network advertises you to thousands, while also giving you a chance to test the service before paying anything at all. All the popular sites are using this service to boost their traffic and ad revenue! Why arenâ€™t you? And what is better than traffic? Itâ€™s recurring traffic! That\'s how running a successful site works... Here\'s to your success! Find out more here: http://zoy.bz/4nm - or to unsubscribe please go here: http://innovad.ws/8h9dp','2015-05-16 04:21:14',1),(12,'Mrs Anita Ford','07891981608','netaford@gmail.com','Hello could you advise if you have 2 double rooms vacant for January/February 2016 and your rates\r\nMany thanks\r\nAnita','2015-05-19 04:59:39',1);
/*!40000 ALTER TABLE `tbl_ContactUs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_CustomerInformation`
--

DROP TABLE IF EXISTS `tbl_CustomerInformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_CustomerInformation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custFirstName` varchar(20) DEFAULT NULL,
  `custLastName` varchar(20) DEFAULT NULL,
  `custContact` varchar(15) DEFAULT NULL,
  `custEmail` varchar(50) DEFAULT NULL,
  `custAddress` varchar(200) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_CustomerInformation`
--

LOCK TABLES `tbl_CustomerInformation` WRITE;
/*!40000 ALTER TABLE `tbl_CustomerInformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_CustomerInformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Facility`
--

DROP TABLE IF EXISTS `tbl_Facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Facility` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facility` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Facility`
--

LOCK TABLES `tbl_Facility` WRITE;
/*!40000 ALTER TABLE `tbl_Facility` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_Facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Reviews`
--

DROP TABLE IF EXISTS `tbl_Reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT '0',
  `review` varchar(2000) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NULL DEFAULT NULL,
  `readFlag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Reviews`
--

LOCK TABLES `tbl_Reviews` WRITE;
/*!40000 ALTER TABLE `tbl_Reviews` DISABLE KEYS */;
INSERT INTO `tbl_Reviews` VALUES (68,'teststst','ssdfsdfsdfdsfsf',5,'2016-09-03 13:16:35',1),(69,'asdasdasdasdas','asdasdasddad',3,'2016-09-04 00:47:11',1);
/*!40000 ALTER TABLE `tbl_Reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_RoomService`
--

DROP TABLE IF EXISTS `tbl_RoomService`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_RoomService` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RoomServiceCategoryId` int(11) DEFAULT NULL,
  `tariff` varchar(20) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_RoomService`
--

LOCK TABLES `tbl_RoomService` WRITE;
/*!40000 ALTER TABLE `tbl_RoomService` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_RoomService` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_RoomServiceCategory`
--

DROP TABLE IF EXISTS `tbl_RoomServiceCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_RoomServiceCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_RoomServiceCategory`
--

LOCK TABLES `tbl_RoomServiceCategory` WRITE;
/*!40000 ALTER TABLE `tbl_RoomServiceCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_RoomServiceCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Stay`
--

DROP TABLE IF EXISTS `tbl_Stay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Stay` (
  `facility` varchar(20) NOT NULL DEFAULT '',
  `image1` varchar(10) DEFAULT NULL,
  `image2` varchar(10) DEFAULT NULL,
  `image3` varchar(20) DEFAULT NULL,
  `image4` varchar(20) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`facility`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Stay`
--

LOCK TABLES `tbl_Stay` WRITE;
/*!40000 ALTER TABLE `tbl_Stay` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_Stay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Tariff`
--

DROP TABLE IF EXISTS `tbl_Tariff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Tariff` (
  `category` varchar(20) NOT NULL DEFAULT '',
  `priceSingle` varchar(10) DEFAULT NULL,
  `priceDouble` varchar(10) DEFAULT NULL,
  `description` varchar(20) DEFAULT NULL,
  `moreDetail` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Tariff`
--

LOCK TABLES `tbl_Tariff` WRITE;
/*!40000 ALTER TABLE `tbl_Tariff` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_Tariff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_UserType`
--

DROP TABLE IF EXISTS `tbl_UserType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_UserType` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_UserType`
--

LOCK TABLES `tbl_UserType` WRITE;
/*!40000 ALTER TABLE `tbl_UserType` DISABLE KEYS */;
INSERT INTO `tbl_UserType` VALUES (0,'Admin'),(1,'Other');
/*!40000 ALTER TABLE `tbl_UserType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_Users`
--

DROP TABLE IF EXISTS `tbl_Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(10) DEFAULT NULL,
  `userPasswd` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `securityQuestion` varchar(500) DEFAULT NULL,
  `securityAnswer` varchar(500) DEFAULT NULL,
  `userTypeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_Users`
--

LOCK TABLES `tbl_Users` WRITE;
/*!40000 ALTER TABLE `tbl_Users` DISABLE KEYS */;
INSERT INTO `tbl_Users` VALUES (1,'admin','bosco_15j67','renoircota@gmail.com',NULL,NULL,0),(2,'superuser','sa123###456','nitinmanju@gmail.com','','',0),(3,'akshay','akshay','asdasd',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-05 14:33:40
