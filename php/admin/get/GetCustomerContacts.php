<?php
header('Content-Type: application/json');

require_once('../../db/mysqlConnect.php');
if($_SESSION["cotas_user"]){
$sql = "select * from tbl_ContactUs order by tbl_ContactUs.visitTime desc";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $rows = array();
      while($r = mysqli_fetch_array($result)) {
        $rows[] = $r;
      }
} else {
    echo json_encode(array("response"=>array("message"=>"No data found in database", "status"=>false)));
}
echo json_encode($rows);

$conn->close();
} else {
    echo json_encode(array("response"=>array("message"=>"Please login to see the data", "status"=>false)));
}
?>
