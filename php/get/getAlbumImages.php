<?php
header('Content-Type: application/json');
$dir = "../../img/gallery/thumbnails";

// Open a directory, and read its contents
function getImagesFromDirectory($dir){
    $images = array();
    if (is_dir($dir)){
        if ($dh = opendir($dir)){
          while (($file = readdir($dh)) !== false){
            $images[] = $dir.'/'.$file;
          }
          closedir($dh);
        }
    }
    return array("data"=>$images);
}

echo json_encode(getImagesFromDirectory($dir));

?>
