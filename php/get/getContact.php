<?php
header('Content-Type: application/json');
/*
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "cota";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
*/
require_once('../db/mysqlConnect.php');

$sql = "select * from tbl_Accommodation";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $rows = array();
      while($r = mysqli_fetch_array($result)) {
        $rows[] = $r;
      }
} else {
    echo json_encode(array("response"=>array("message"=>"Error Occurred", "status"=>false)));
}
echo json_encode($rows);

$conn->close();
?>