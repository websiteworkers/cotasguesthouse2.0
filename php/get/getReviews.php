<?php 
header('Content-Type: application/json');
require_once '../db/mysqlConnect.php';
$sql = "select tbl_Reviews.id, tbl_Reviews.title, tbl_Reviews.review, tbl_Reviews.rating, (UNIX_TIMESTAMP(tbl_Reviews.date)*1000) as time, tbl_Reviews.readFlag from tbl_Reviews order by tbl_Reviews.date desc ";
if($_GET['page']!=null){
    $sql= $sql." limit 10 OFFSET ".($_GET['page']*10);
}
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    $rows = array();
      while($r = mysqli_fetch_array($result)) {
        $rows[] = $r;
      }
} else {
    $rows= array("status"=> false);
}
echo json_encode($rows);
?>