<?php

function createThumbnail($path, $save, $width, $height){
    $info = getimagesize($path);
    $size = array($info[0], $info[1]);
    
    $thumb = imagecreatetruecolor($width, $height);
    
    
    $src_aspect = $size[0]/$size[1];
    $thumb_aspect = $width/$height;
    
    if($info['mime'] == 'image/png'){
        $src = imagecreatefrompng($path);
    } else if($info['mime'] == 'image/jpeg'){
        $src = imagecreatefromjpeg($path);
    } else if($info['mime'] == 'image/gif'){
        $src = imagecreatefromgif($path);
    } else {
        return false;
    } 
    if($src_aspect < $thumb_aspect){
        //narrow
        $scale = $width/$size[0];
        $new_size = array($width, $width/$src_aspect);
        $src_pos = array(0, ($size[1] * $scale-$height)/$scale/2);
    } elseif ($src_aspect > $thumb_aspect) {
        //wide
        $scale = $width/$size[0];
        $new_size = array($height * $src_aspect, $height);
        $src_pos = array(($size[0] * $scale - $width)/ $scale / 2, 0);
    } else {
        //normal
        $new_size = array($width, $height);
        $src_pos = array(0, 0);
    }
    $new_size[0] = max($new_size[0], 1);
    $new_size[1] = max($new_size[1], 1);
    //echo $thumb." -- ".$src." -- 0 -- 0 -- ".$src_pos[0]." -- ".$src_pos[1]." -- ".$new_size[0]." -- ".$new_size[1]." -- ".$size[0]." -- ".$size[1];
    imagecopyresampled($thumb, $src, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $size[0], $size[1]);
    if($save==false){
        imagepng($thumb);
    } else {
        echo $save;
        imagejpeg($thumb, $save);
    }
    
}

?>