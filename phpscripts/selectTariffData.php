<?php

include 'oc.php';

$result = mysqli_query($con,"SELECT * FROM tbl_Tariff");

echo "<table class=\"table table-striped\">
	  <tr>
      <th>Category</th>
      <th>Per Head</th>
      <th>Couples</th>
      <th>Remarks</th>
      <th></th>
    </tr>";

while($row = mysqli_fetch_array($result)) 
{
	echo"
    <tr>
      <td>". $row['category'] ."</td>
      <td>". $row['priceSingle'] ."</td>
      <td>". $row['priceDouble'] ."</td>
      <td>". $row['description'] ."</td>
    </tr>";
}

echo "</table>";

include 'clsc.php';
?>