<?php

ob_start();
	$dir = "photoscroller/";
	
	// Sort in ascending order - this is default
	$imgnamearr = scandir($dir);
	
	$firstTime = 0;
	
	foreach ($imgnamearr as &$value) 
	{
	    if (strpos($value, '.jpg') !== false  
	     || strpos($value, '.png') !== false 
	     || strpos($value, '.bmp') !== false
		 || strpos($value, '.JPG') !== false
		 || strpos($value, '.BMP') !== false
		 || strpos($value, '.PNG') !== false
		 || strpos($value, '.jpeg') !== false
		 || strpos($value, '.JPEG') !== false
		 || strpos($value, '.gif') !== false
		 || strpos($value, '.GIF') !== false)
		{
			
			if($firstTime == 0)
			{
				echo "<div class=\"item active\">";
				$firstTime = 1;
			}
			else 
			{
				echo "<div class=\"item\">";
			}
			
			
//			echo "<img class=\"img-responsive img-thumbnail\" id=\"thumbnail-image\" hspace=\"10\" vspace=\"10\" width=\"100\" height=\"100\" src=" . $dir ."/".$value.">";
			echo "<img src=" . $dir ."".$value.">";
			echo "</div>";
		}
	}
	
	ob_end_flush();
?>