'use strict';
/**
 * Created by akshay on 31/1/16.
 */
var cotasApp= angular.module("cotasApp",[
    'ngRoute',
    'ngAnimate',
    /*'ngResource',*/
    'pageControllers',
    'ui.bootstrap',
    'pageDirectives',
    'factorySettings',
    'infinite-scroll',
    'ngSanitize'
]);

cotasApp.config(["$routeProvider",
    function($routeProvider){
        $routeProvider.
            when("/about", {
                templateUrl:"partials/about.html",
                controller: 'aboutController'
            }).
            when("/reviews",{
                templateUrl:"partials/reviews.html",
                controller: 'reviewsController'
            }).
            when("/gallery",{
                templateUrl:"partials/gallery-old.html",
                controller: 'galleryController'
            }).
            when("/contact",{
                templateUrl:"partials/contact.html",
                controller: 'contactController'}).
            otherwise({redirectTo:"about"})
}]);
