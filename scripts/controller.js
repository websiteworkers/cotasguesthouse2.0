/**
 * Created by akshay on 16/3/16.
 */

    'use strict';
    var controllers=    angular.module("pageControllers",[]);

controllers.controller('aboutController', function ($scope, $http, $sce) {
        $scope.pageClass = 'page-about';
        //$http.get("http://localhost/conn.php");
        $http({
            method: 'GET',
            url: 'php/get/getAbout.php'
        }).then(function successCallback(response) {
            $scope.aboutData=response.data[0];
        }, function errorCallback(response) {
            $scope.errorMessage="Failed To retrieve data, Sorry for the inconvenience";
        });

        $http({
            method: "GET",
            url: 'php/get/getCarouselImages.php'
        }).then(function success(response){
            var rawData=response.data;
            $scope.carouselImages=[];
            rawData.data.forEach(function(img){
                if(img.substr(img.lastIndexOf(".")+1).length>2){
                    $scope.carouselImages.push(img.replace("../../",""));
                }
            });
        }, function failure(response){

        });
        /*$scope.aboutData = [
            {
                key: 1,
                data: " dfadasdas adasdasd addasdasdasdas  asdasd aadasddas   Cota Residency offers the best in affordable lodging. Just a short walk from the golden sands of Arossim beach. Enjoy the convenience of our location next to the Park Hyatt Goa Resort and Spa. We offer luxury accommodations at affordable prices! Visit us on :Face book- Cota Residency Review us on : Trip advisor- Cota Residency-Utorda Call us :+91-9822162106"
            }, {
                key: 2,
                data: "Description Cota Residency includes beautifully appointed and well furnished Air conditioned suites. All suites offer the amenities you would expect, including a kitchenette, with a large fridge, microwave, electric kettle, dishes, flat screen TVs with cable package and independent balconies. A warm and friendly family awaits you at the Cota Residency! *12km from Dabolim Airport (GOI) *10 minute walk from the beach. *Walking distance to local churches. *Walking distance to local /starred restaurants Description"
            }
        ]*/
    }).directive("showHtml", function(){
    return {
        restrict : 'A',
        template : "dfdsfsdfsdfsdfdsfdsfsdfds"
    }
});


controllers.controller('reviewsController', ["$scope","$uibModal", "$log", "$http", "GetReviewsService", function( $scope, $uibModal, $log, $http, GetReviewsService) {
        //$scope.page=1;
        $scope.readonlyFlag=true;
        $scope.$on('loadReviews', function (event, args) {
            $scope.message = args.message;
            console.log($scope.message);
            $http({
                method :"GET",
                url : "php/get/getReviews.php?page="+0
            }).then(function success(response){
                $scope.reviewsData = response.data;
            }, function failure(response){
                $scope.failureMessage = response;
            });
            //loadReviews();
        });
        $scope.addMoreItems= function (){
            console.log("scrolling.....");
            $http({
                method :"GET",
                url : "php/get/getReviews.php?page="+$scope.page
            }).then(function success(response){
                $scope.reviewsData.push(response.data);
            }, function failure(response){
                $scope.failureMessage = response;
            });
        }

        $scope.more = function(page){
            //console.log("scrolling.....");
            if(!$scope.page){
                $scope.page=1;
            } else {
                $scope.page++;
            }

            $http({
                method :"GET",
                url : "php/get/getReviews.php?page="+$scope.page
            }).then(function success(response){
                response.data.forEach(function(item){
                    $scope.reviewsData.push(item);
                });
            }, function failure(response){
                $scope.failureMessage = response;
            });
        };
        /*$scope.$on('loadReviews', function(event, args){
            $scope.message = args.message;
            console.log($scope.message);
        });*/
        //GetReviewsService.getReviews($scope.page);
        $http({
            method :"GET",
            url : "php/get/getReviews.php?page="+0
        }).then(function success(response){
            $scope.reviewsData = response.data;
        }, function failure(response){
            $scope.failureMessage = response;
        });
        $scope.pageClass = 'page-home';
        $scope.animationsEnabled = true;
        $scope.items = [1,2,3,4,5];
        $scope.stars=[1,2,3,4,5];
        $scope.open = function (size) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        $scope.selectStars = function (value) {
            console.log("adsasd adasdas dsad asdsa : "+value);
        }

    }]);


controllers.controller('contactController', function($scope, $http) {
        $scope.pageClass = 'page-contact';
        $http({
            method: 'GET',
            url: 'php/get/getContact.php'
        }).then(function successCallback(response) {
            $scope.contactData=response.data[0];
        }, function errorCallback(response) {
            $scope.errorMessage="Failed To retrieve data, Sorry for the inconvenience";
        });
        $scope.contactUs = function(){
            if(!$scope.contact){
                $scope.nameValidationFailed=true;
                $scope.emailValidationFailed=true;
                $scope.messageValidationFailed=true;
                return;
            }
            if(!$scope.contact.name){
                $scope.nameValidationFailed=true;
            } else {
                $scope.nameValidationFailed=false;
            }
            if(!$scope.contact.email){
                $scope.emailValidationFailed=true;
            } else {
                $scope.emailValidationFailed=false;
            }
            if(!$scope.contact.message){
                $scope.messageValidationFailed=true;
            } else {
                $scope.messageValidationFailed=false;
            }
            if($scope.contact.name && $scope.contact.email && $scope.contact.message){
                $http({
                    method: 'POST',
                    url: 'php/post/postContact.php',
                    data:[$scope.contact]
                }).then(function success(response){
                    console.log("success call back contact post");
                    $scope.contact=null;
                }, function(error){
                    console.log("failure call back contact post");
                })
            }
        }
    });

controllers.controller('galleryController', function($scope,$http, $uibModal, $log) {
    $scope.imageDirPath = "img/gallery/";
    $http({
        method: 'GET',
        url: 'php/get/getAlbumImages.php'
    }).then(function successCallback(response) {

        var rawData=response.data;
        $scope.images=[];
        rawData.data.forEach(function(img){
            if(img.substr(img.lastIndexOf(".")+1).length>2){
                $scope.images.push(img.replace("../../",""));
            }
        });
    }, function errorCallback(response) {
        $scope.errorMessage="Failed To retrieve data, Sorry for the inconvenience";
    });

    $scope.getFullImage=function(thumbnail, size){
        var imageFileName = thumbnail.substr(thumbnail.indexOf("_")+1);
        //alert(imageFileName);
        $scope.fullImageFilePath =  $scope.imageDirPath+imageFileName;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'imageModal',
            controller: "AlbumModalController",
            size: size,
            windowClass : "modalAlbum",
            resolve: {
                currentImage: function(){
                    return $scope.fullImageFilePath
                },
                albumImages: function(){
                    return $scope.images;
                },
                imageDirPath: function(){
                    return $scope.imageDirPath;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
        });
    }

    });

controllers.controller('AlbumModalController',function ($scope, currentImage, $window, albumImages, imageDirPath) {
        $scope.currentImage = currentImage;
        $scope.albumImages = albumImages;
        $scope.height1="asd";
        $scope.currentImageIndex;
    $('#imageModal')
        $scope.scrollImage=function(direction){
            var currentImageIndex= getCurrentImageIndex($scope.currentImage);
            if(direction=="left"){
                if(currentImageIndex==0){
                    $scope.scrollTo=$scope.albumImages.length-1;;
                } else {
                    $scope.scrollTo=currentImageIndex-1;
                }
            } else {
                if(currentImageIndex==$scope.albumImages.length-1){
                    $scope.scrollTo=0
                } else {
                    $scope.scrollTo=currentImageIndex+1;
                }

            }
            var thumbNailImage = albumImages[$scope.scrollTo];
            var imageFileName = thumbNailImage.substr(thumbNailImage.indexOf("_")+1);
            //alert(imageFileName);
            $scope.currentImage =  imageDirPath+imageFileName;
            $scope.$apply();
        }

        $scope.keyScrollImage = function($event){
            console.log($event);
        }

        function getCurrentImageIndex(currentImage){
            var imgIndex;
            $scope.albumImages.forEach(function(item, index) {
                if (item.indexOf(currentImage.substr(currentImage.lastIndexOf("/")+1)) !=-1) {
                    imgIndex= index;
                }
            });
            return imgIndex;
        }
        $scope.myFunct = function(keyEvent) {
            if (keyEvent.which === 37) {
                $scope.scrollImage('left');
            }
            else if (keyEvent.which === 39) {
                $scope.scrollImage('right');
            }
        };
        /*jQuery(".container-fluid").keydown(function (event) {
            if (event.keyCode == 37) {
                $scope.scrollImage('left');
            } else if (event.keyCode == 39) {
                $scope.scrollImage('right');
            }
        });*/
    $('.container-fluid').bind('keydown', function(event) {
        //console.log(event.keyCode);
        switch(event.keyCode){
            case '37': console.log("asdasdasdasdas");break;
            default :break;
        }
    });

    $(document.documentElement).keyup(function (event) {
        if (event.keyCode == 37) {
            $scope.scrollImage('left');
        } else if (event.keyCode == 39) {
            $scope.scrollImage('right');
        }
    });
});

controllers.controller('ModalDemoCtrl', function ($scope, $uibModal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

});

controllers.controller('ModalInstanceCtrl', function ($scope, $rootScope, $http, $uibModalInstance, $timeout, items, GetReviewsService) {

    $scope.items = items;
    $scope.selected = {
        item: $scope.items[0]
    };
    $scope.ratedValue=0;
    $scope.ok = function () {
        console.log("rating value,... "+$scope.ratedValue);
        if($scope.ratedValue && $scope.message){
            $scope.starValidateFail=false;
            $scope.messageValidateFail=false;
            $scope.review={
                time: new Date(),
                message: $scope.message,
                titleText: $scope.titleText,
                rating: $scope.ratedValue
            }
            $scope.review.time= new Date();
            $http({
                url:"php/post/postReview.php",
                method: "POST",
                data: [$scope.review]
            }).then(function success(response){
                $scope.successMessage=response.data;
                $scope.responseStatus=true;
                $timeout(function(){
                    $uibModalInstance.close($scope.selected.item);
                    //$scope.$emit('loadReviews', { message: $scope.successMessage });
                    $rootScope.$broadcast("loadReviews", response.data);
                }, 1000);
            }, function failure(response){
                $scope.responseStatus=false;
                $scope.successMessage="Some error occurred while saving the review, sorry for the inconvinience.. ";
            });            
        } else {
            if(!$scope.ratedValue){
                $scope.starValidateFail=true;
            } else {
                $scope.starValidateFail=false;
            } 
            if(!$scope.message){
                $scope.messageValidateFail=true;
            } else {
                $scope.messageValidateFail=false;
            }
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});

controllers.controller("navControl", function($scope, $location){
    $scope.isActive = function(viewLocation){
        var active = (viewLocation === $location.path());
        return active;
    }
});