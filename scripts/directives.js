/**
 * Created by akshay on 20/3/16.
 */

//var directives= angular.module("pageDirectives",[]);

var directives=angular.module("pageDirectives",[]);
directives.directive("starrating", function(){
    return {
        restrict : 'A',
        template :'<ul class="list-inline">'
    + ' <li ng-repeat="star in stars" ng-class="{filled: $index<=selectedIndex, filled:$index<rating}" ng-click="toggleIndex($index)">'
        + '  <i class="glyphicon glyphicon-star fa-2x"></i>'
        + ' </li>'
        + '</ul>',
        scope : {
            rating : '=',
            time : '=',

    },
        link: function (scope) {
            scope.stars=[1,2,3,4,5];
            scope.toggleIndex= function(index){
                if(!scope.time){
                    scope.selectedIndex=index;
                    scope.rating=index+1;
                }

            }
        }
};
});
directives.directive("customdir", function(){
    return {
        template:'<div></div>',
        restrict: 'A',
        scope : {
            value : '='
        },
        link: function(scope, element, attr){
           scope.$watch('value',function(tel){
                element.html(scope.value);  // 1234
            });
        }
    }
});;
