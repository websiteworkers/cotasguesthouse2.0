/**
 * Created by akshay on 24/3/16.
 */
var CotasServices = angular.module('factorySettings',[]);

CotasServices.factory('GetReviewsService', function($rootScope,$http){
    return {
        getReviews : function (page) {
            $http({
                method :"GET",
                url : "php/getReviews.php?page="+page
            }).then(function success(response){
                //$scope.reviewsData = response.data;
                //return response.data;
            }, function failure(response){
                return  response;
            });
        }
    }
});
/*

var myModule = angular.module('factorySettings', []);
myModule.factory('mySharedService', function($rootScope) {
    var sharedService = {};

    sharedService.ratedStar = '';

    sharedService.prepForBroadcast = function(msg) {
        this.ratedStar = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
        alert("broadcasting.... ");
    };

    return sharedService;
});*/
