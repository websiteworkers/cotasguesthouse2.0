/**
 * Created by akshay on 27/2/16.
 */
$(".gallery-col").on("click", function() {
    $('.imagepreview').attr('src', $('.img-responsive').attr('src')); // here asign the image to the modal when the user click the enlarge link
    $('#myModal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});
$("#modal-carousel").carousel({interval:false});

/* change modal title when slide changes */
$("#modal-carousel").on("slid.bs.carousel",       function () {
    $(".modal-title")
        .html($(this)
            .find(".active img")
            .attr("title"));
});

/* when clicking a thumbnail */
$(".row .thumbnail").click(function(){
    var content = $(".carousel-inner");
    var title = $(".modal-title");

    content.empty();
    title.empty();

    var id = this.id;
    var repo = $("#img-repo .item");
    var repoCopy = repo.filter("#" + id).clone();
    var active = repoCopy.first();

    active.addClass("active");
    title.html(active.find("img").attr("title"));
    content.append(repoCopy);

    // show the modal
    $("#modal-gallery").modal("show");
});